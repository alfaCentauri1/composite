package compositePatron;

public interface IMenu {
    /***/
    boolean open();
    /***/
    boolean close();
    /***/
    void accion();
}
