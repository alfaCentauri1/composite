package compositePatron;
/**
 * Implementa el método main para la ejecución del programa.
 * @author Ricardo Presilla.
 * @version 1.0.
 */
public class Main {

    public static void main(String[] args) {
	    System.out.println("Ejemplo del patron estructural Composite.");
        Menu menu = new Menu();
        Menu menu2 = new Menu();
        Menu menu3 = new Menu();
        Menu menu4 = new Menu();
        Menu menu5 = new Menu();
        Menu menu6 = new Menu();

        menu.addMenu(menu2);
        menu.addMenu(menu3);
        menu4.addMenu(menu5);
        menu4.addMenu(menu6);

        menu.open();
        menu.accion();
        menu.close();

        menu4.open();
        menu4.accion();
        menu4.close();
    }
}
