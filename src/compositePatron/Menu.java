package compositePatron;

import java.util.ArrayList;
/**
 * Implementa la interfaz IMenu y la logica de negocios.
 * @author Ricardo Presilla.
 * @version 1.0.
 */
public class Menu implements IMenu{
    private ArrayList<IMenu> menus;
    /**
     * Contructor, instancia los parametros necesarios.
     **/
    public Menu() {
        this.menus = new ArrayList<IMenu>();
    }

    /**
     * Método para indicar apertura del menu.
     * @return Regresa un booleano.
     */
    @Override
    public boolean open() {
        System.out.println("Abierto!");
        return true;
    }

    /**
     * Método para indicar cierre del menu.
     * @return Regresa un booleano.
     */
    @Override
    public boolean close() {
        System.out.println("Cerrado!");
        return true;
    }

    /**
     * Método para indicar acción del menu.
     */
    @Override
    public void accion() {
        System.out.println("Acción....");
    }

    /**
     * Agrega un objeto a la lista de objetos hijos del menu.
     * @param menu Tipo IMenu.
     */
    public void addMenu(IMenu menu){
        this.menus.add(menu);
    }

    /**
     * Obtiene un submenu de la lista.
     * @param posicion Tipo entero.
     * @return Regresa un objeto de tipo IMenu.
     */
    public IMenu getMenu(int posicion){
        return this.menus.get(posicion);
    }
}
